import React from 'react';
import BitcoinDetailContainer from './BitcoinDetailContainer';

/**
 * 선택한 비트코인의 세부정보를 보여주는 컴포넌트
 */
const BitcoinDetail = () => <BitcoinDetailContainer />;

export default BitcoinDetail;
