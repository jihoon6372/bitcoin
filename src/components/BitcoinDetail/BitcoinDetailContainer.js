import { connect } from 'react-redux';
import BitcoinDetailPresentational from './BitcoinDetailPresentational';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

const BitcoinDetailContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BitcoinDetailPresentational);

export default BitcoinDetailContainer;
