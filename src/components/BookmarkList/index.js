import React from 'react';
import BookmarkListContainer from './BookmarkListContainer';

/**
 * 비트코인 종목 중에서 즐겨찾기 등록한 리스트들 보여주는 컴포넌트
 */
const BookmarkList = () => <BookmarkListContainer />;

export default BookmarkList;
