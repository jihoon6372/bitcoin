import { connect } from 'react-redux';
import BookmarkListPresentational from './BookmarkListPresentational';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

const BookmarkListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookmarkListPresentational);

export default BookmarkListContainer;
