import React from 'react';
import './App.css';
import BitcoinDetail from './BitcoinDetail';
import BookmarkList from './BookmarkList';
import MarketList from './MarketList';

const App = () => (
  <div style={{ overflow: 'auto', height: 600, border: 'solid 1px' }}>
    <div
      style={{
        float: 'left',
        width: 'calc(70% - 2px )',
        textAlign: 'center',
        borderRight: 'solid 1px',
      }}
    >
      <BitcoinDetail />
      <BookmarkList />
    </div>
    <div style={{ float: 'left', height: '100%', width: '30%' }}>
      <MarketList />
    </div>
  </div>
);

export default App;
