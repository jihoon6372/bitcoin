import React from 'react';
import MarketListContainer from './MarketListContainer';

/**
 * 종목 리스트 컴포넌트
 */
const MarketList = () => <MarketListContainer />;

export default MarketList;
