import { connect } from 'react-redux';
import MarketListPresentational from './MarketListPresentational';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

const MarketListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketListPresentational);

export default MarketListContainer;
